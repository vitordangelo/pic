
_main:

;Led Simples Piscando.c,7 :: 		void main()
;Led Simples Piscando.c,9 :: 		TRISC=0xFF;
	MOVLW      255
	MOVWF      TRISC+0
;Led Simples Piscando.c,10 :: 		TRISD=0;
	CLRF       TRISD+0
;Led Simples Piscando.c,12 :: 		while (1)
L_main0:
;Led Simples Piscando.c,14 :: 		if (CH1 == LIG)
	BTFSS      PORTC+0, 0
	GOTO       L_main2
;Led Simples Piscando.c,16 :: 		LED0=OFF;
	BCF        PORTD+0, 0
;Led Simples Piscando.c,17 :: 		LED1=ON;
	BSF        PORTD+0, 1
;Led Simples Piscando.c,18 :: 		}
	GOTO       L_main3
L_main2:
;Led Simples Piscando.c,22 :: 		LED0=ON;
	BSF        PORTD+0, 0
;Led Simples Piscando.c,23 :: 		LED1=OFF;
	BCF        PORTD+0, 1
;Led Simples Piscando.c,24 :: 		}
L_main3:
;Led Simples Piscando.c,26 :: 		}
	GOTO       L_main0
;Led Simples Piscando.c,27 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
