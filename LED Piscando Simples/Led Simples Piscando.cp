#line 1 "H:/Eletrônica - Programação/PIC/LED Piscando Simples/Led Simples Piscando.c"




int const LIG=1, ON=1, OFF=0;

void main()
{
 TRISC=0xFF;
 TRISD=0;

 while (1)
 {
 if ( PORTC.RC0  == LIG)
 {
  PORTD.RD0 =OFF;
  PORTD.RD1 =ON;
 }

 else
 {
  PORTD.RD0 =ON;
  PORTD.RD1 =OFF;
 }

 }
}
