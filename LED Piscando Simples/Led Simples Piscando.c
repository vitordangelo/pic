#define LED0 PORTD.RD0
#define LED1 PORTD.RD1
#define CH1 PORTC.RC0

int const LIG=1, ON=1, OFF=0;

void main()
{
	TRISC=0xFF;
	TRISD=0;

	while (1)
	{
		if (CH1 == LIG)
		{
			LED0=OFF;
			LED1=ON;
		}

		else
		{
			LED0=ON;
			LED1=OFF;
		}

	}
}