#include "C:\Users\Vitor\Desktop\Tutorial - 19 - RS232 03\Code.h"


void main()
{
   int i;
   int y[12];

   setup_adc_ports(AN0_TO_AN1|VSS_VREF);
   setup_adc(ADC_CLOCK_INTERNAL);
   setup_spi(SPI_SS_DISABLED);
   setup_wdt(WDT_OFF);
   setup_timer_0(RTCC_INTERNAL);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_timer_3(T3_DISABLED|T3_DIV_BY_1);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

   printf("\nInicio\r");
   while(true){

      for (i = 0; i < 10; ++i)
      {
         y[i] = getc();
         printf("%s \n",y);

      }

      printf("%s \n",y);

   }

}
