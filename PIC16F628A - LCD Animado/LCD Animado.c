#include <16F628A.h>
#device adc=8
#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                       //Crystal osc <= 4mhz for PCM/PCH , 3mhz to 10 mhz for PCD
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOBROWNOUT               //No brownout reset
#FUSES MCLR                     //Master Clear pin enabled
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES RESERVED                 //Used to set the reserved FUSE bits
#use delay(clock=4000000)

#include <LCDNew.c>

void main()
{

   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_ccp1(CCP_OFF);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

   int i, j=16;
   lcd_init(); 

   Output_high(pin_b3);

   while(true)
   {

   for (i = 0; i < 16; ++i)
   {
      --j;
      lcd_gotoxy(i,1);
      printf(lcd_putc, "*");
      lcd_gotoxy(j,0);
      printf(lcd_putc, "*");
      delay_ms(300);
   }
   
   lcd_gotoxy(4,1);
   delay_ms(500);
   printf(lcd_putc, "Bem Vindo");  
   delay_ms(1000);
      
   }
}





   
         
         
