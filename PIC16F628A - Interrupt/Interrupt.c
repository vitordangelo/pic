#include <16F628A.h>

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                       //Crystal osc <= 4mhz for PCM/PCH , 3mhz to 10 mhz for PCD
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOBROWNOUT               //No brownout reset
#FUSES MCLR                     //Master Clear pin enabled
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES RESERVED                 //Used to set the reserved FUSE bits

#use delay(clock=4000000)

#use fast_io(b)
#use fast_io(a)
#INT_EXT

#define LEDACESSO output_b(0b00000100);

 void teste(void)
   {
      while(input(pin_b0) == 0)
      {
         LEDACESSO;
        //output_b(0b00000100); 
        output_a(0b00000001); 
      }
   }




void main()
{

   setup_timer_0(RTCC_EXT_H_TO_L|RTCC_DIV_64);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_ccp1(CCP_OFF);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);
   enable_interrupts(INT_RTCC);
   enable_interrupts(GLOBAL);

    set_tris_a(0b00000000);
    set_tris_b(0b00000001);

    ext_int_edge(0,H_TO_L);
    enable_interrupts(global);
    enable_interrupts(INT_EXT);

   while(true)
   {
      output_b(0b0000010); 
      output_a(0b0000000);
      delay_ms(150);
      output_b(0b0001000); 
      delay_ms(150);
   }

}
