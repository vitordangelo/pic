#include "H:\Eletrônica - Programação\PIC\LCD\LCD.h"
#include <_LCD.c>

int temperatura=0;
int valorLido=0;  
int i;
int j=16;    

void main()
{
                                    
   setup_adc(ADC_CLOCK_INTERNAL);
   setup_adc_ports(ALL_ANALOG);
   setup_spi(SPI_SS_DISABLED);    
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DIV_BY_16, 255,1);
   setup_ccp1(CCP_PWM);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE); 
   set_adc_channel(0);
   
   lcd_init();                                                     

   while(true)           
   {
      Output_high(pin_b1);

     
   for (i = 0; i < 16; ++i)
   {
      --j;
      lcd_gotoxy(i,1);
      printf(lcd_putc, "*");
      lcd_gotoxy(j,0);
      printf(lcd_putc, "*");
      delay_ms(300);                            
   }
      lcd_gotoxy(4,1);
       delay_ms(500);                  
       printf(lcd_putc, "Bem Vindo");  
       delay_ms(1000);

      valorLido=read_adc();
      temperatura=valorLido*2;

      while(temperatura < 20)
      {
         valorLido=read_adc();
         temperatura=valorLido*2;
   
         printf(lcd_putc, "\f Temp:  %u",temperatura);  
        
         Output_high(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(300);

         Output_high(pin_c4);
         Output_high(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(300);

         Output_low(pin_c4);
         Output_high(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(300);

         Output_low(pin_c4);
         Output_high(pin_c3);
         Output_high(pin_c1);
         Output_low(pin_c0);
         delay_ms(300);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_high(pin_c1);
         Output_low(pin_c0);
         delay_ms(300);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_high(pin_c1);
         Output_high(pin_c0);
         delay_ms(300);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_high(pin_c0);
         delay_ms(300);

         Output_high(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_high(pin_c0);
         delay_ms(300);

         set_pwm1_duty(255);
       
      }
      while(temperatura > 20)
      {
         valorLido=read_adc();
         temperatura=valorLido*2;
   
         printf(lcd_putc, "\f Temp:  %u",temperatura);  
        
         Output_high(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(50);

         Output_high(pin_c4);
         Output_high(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(50);

         Output_low(pin_c4);
         Output_high(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(50);

         Output_low(pin_c4);
         Output_high(pin_c3);
         Output_high(pin_c1);
         Output_low(pin_c0);
         delay_ms(50);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_high(pin_c1);
         Output_low(pin_c0);
         delay_ms(50);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_high(pin_c1);
         Output_high(pin_c0);
         delay_ms(50);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_high(pin_c0);
         delay_ms(50);

         Output_high(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_high(pin_c0);
         delay_ms(50);  

         set_pwm1_duty(127);
      }



      


                          
   }

}        
