#include <16F877A.h>
#device adc=10
#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                       //Crystal osc <= 4mhz for PCM/PCH , 3mhz to 10 mhz for PCD
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NODEBUG                  //No Debug mode for ICD
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES NOWRT                    //Program memory not write protected
#FUSES RESERVED                 //Used to set the reserved FUSE bits
#use delay(clock=4000000)

void main()
{
   
   setup_adc(ADC_CLOCK_INTERNAL);
   setup_adc_ports(ALL_ANALOG);
   setup_spi(SPI_SS_DISABLED);    
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DIV_BY_16, 255,1);
   setup_ccp1(CCP_PWM);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE); 
   set_adc_channel(0);

   float valorLido;
   float temperatura;  
   float tempLCD; 

   while(true)
   {  
      
      valorLido=read_adc();
      temperatura=valorLido*0.0048/0.01+0.5;

      if (temperatura>=15 && temperatura <16)      
        {
           Output_high(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_low(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_high(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_high(pin_b6);
           Output_low(pin_b7);

        }  

      if (temperatura>=16 && temperatura <17)      
        {
        
           Output_high(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_low(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_low(pin_b4); //Menos Siginificativo 
           Output_high(pin_b5);
           Output_high(pin_b6);
           Output_low(pin_b7);

        } 

        if (temperatura>=17 && temperatura <18)      
        {
           Output_high(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_low(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_high(pin_b4); //Menos Siginificativo 
           Output_high(pin_b5);
           Output_high(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=18 && temperatura <19)      
        {
           Output_high(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_low(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_low(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_low(pin_b6);
           Output_high(pin_b7);

        }

        if (temperatura>=19 && temperatura <20)      
        {
           Output_high(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_low(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_high(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_low(pin_b6);
           Output_high(pin_b7);

        }

        if (temperatura>=20 && temperatura <21)      
        {
           Output_low(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_low(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_low(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=21 && temperatura <22)      
        {
           Output_low(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_high(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_low(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=22 && temperatura <23)      
        {
           Output_low(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_low(pin_b4); //Menos Siginificativo 
           Output_high(pin_b5);
           Output_low(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=23 && temperatura <24)      
        {
           Output_low(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_high(pin_b4); //Menos Siginificativo 
           Output_high(pin_b5);
           Output_low(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=24 && temperatura <25)      
        {
           Output_low(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_low(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_high(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=25 && temperatura <26)      
        {
           Output_low(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_high(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_high(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=26 && temperatura <27)      
        {
           Output_low(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_low(pin_b4); //Menos Siginificativo 
           Output_high(pin_b5);
           Output_high(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=27 && temperatura <28)      
        {
           Output_low(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_high(pin_b4); //Menos Siginificativo 
           Output_high(pin_b5);
           Output_high(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=28 && temperatura <29)      
        {
           Output_low(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_low(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_low(pin_b6);
           Output_high(pin_b7);

        }

        if (temperatura>=29 && temperatura <30)      
        {
           Output_low(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_high(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_low(pin_b6);
           Output_high(pin_b7);

        }

        if (temperatura>=30 && temperatura <31)      
        {
           Output_high(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_low(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_low(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=31 && temperatura <32)      
        {
           Output_high(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_high(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_low(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=32 && temperatura <33)      
        {
           Output_high(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_low(pin_b4); //Menos Siginificativo 
           Output_high(pin_b5);
           Output_low(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=33 && temperatura <34)      
        {
           Output_high(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_high(pin_b4); //Menos Siginificativo 
           Output_high(pin_b5);
           Output_low(pin_b6);
           Output_low(pin_b7);

        }

        if (temperatura>=34 && temperatura <35)      
        {
           Output_high(pin_b0); //Menos Siginificativo - Ativa com 1
           Output_high(pin_b1);
           Output_low(pin_b2);
           Output_low(pin_b3);

           Output_low(pin_b4); //Menos Siginificativo 
           Output_low(pin_b5);
           Output_high(pin_b6);
           Output_low(pin_b7);

        }

         



   }

}
