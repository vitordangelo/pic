#include <16F628A.h>

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                       //Crystal osc <= 4mhz for PCM/PCH , 3mhz to 10 mhz for PCD
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOBROWNOUT               //No brownout reset
#FUSES MCLR                     //Master Clear pin enabled
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES RESERVED                 //Used to set the reserved FUSE bits

#use delay(clock=4000000)

void LedErro(void) //Senha Incorreta
{
   unsigned long int i;
   
   for(i=0; i<30; i++)
   {
      Output_high(pin_b6);
      delay_ms(100);
      Output_low(pin_b6);
      delay_ms(100); 
      //printf(lcd_putc, " \f Senha Errada!");  
   }
   //printf(lcd_putc, " \f Senha!"); 
   Output_low(pin_b5);
}
void LedOk(void) //Senha Correta
{
   unsigned long int i;
   
   for(i=0; i<30; i++)
   {
      Output_high(pin_b7);
      delay_ms(100);
      Output_low(pin_b7);
      delay_ms(100); 
      //printf(lcd_putc, " \f Senha Correta!");   
   }
   //printf(lcd_putc, " \f Senha!"); 
   Output_high(pin_b5);
}

unsigned char LerBotoes(void)
{
   if(input(pin_a0) == 0)
      return 0;
   
   if(input(pin_a1) == 0)
      return 1;
      
   if(input(pin_a2) == 0)
      return 2;
      
   if(input(pin_a3) == 0)
      return 3;

   return 0xFF;
}


void main (void)
{
   unsigned char i,j;
   unsigned char Senha[4];
   unsigned char Digito[4];

   if(read_eeprom(0) == 0xFF && read_eeprom(1) == 0xFF && read_eeprom(2) == 0xFF && read_eeprom(3) == 0xFF) //Senha pré definida, memória LIMPA.
   {
      Senha[0] = 0;
      Senha[1] = 1;
      Senha[2] = 2;
      Senha[3] = 3;
   }

   else
   {
      for(i=0; i<4; i++)
      {
         Senha[i] == read_eeprom(i);
      }   
   }

   while(true)
   {
      
      do //Leitura do digito 0
      {
         i = LerBotoes();
      }  
      while(i == 0xFF);
      
      delay_ms(300);
      Digito[0] = i;

      do //Leitura do digito 1
      {
         i = LerBotoes();
      }
      while(i == 0xFF);
      
      delay_ms(300);
      Digito[1] = i;

      do //Leitura do digito 2
      {
         i = LerBotoes();
      }
      while(i == 0xFF);
      
      delay_ms(300);
      Digito[2] = i;

      do //Leitura do Digito 3
      {
         i = LerBotoes();
      }
      while(i == 0xFF);
      
      delay_ms(300);
      Digito[3] = i;


       if(input(pin_a4) == 0)//Grava nova senha
      {
         for(i=0; i<4; i++)
         {
            Senha[i] = Digito[i];
            write_eeprom(i,Digito[i]);
         }
      }

      j = 0; //Verifica a senha
      for(i=0; i<4; i++)
      {
         if(Senha[i] == Digito[i])
            j++;
      }
      
      if(j == 4)        //se senha correta
      {
        LedOk();
      }
      else              //se senha errada
      {
         LedErro();
      }

   }
}