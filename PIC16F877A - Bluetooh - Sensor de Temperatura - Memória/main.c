#include <16F877A.h>
#device adc=10

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                       //Crystal osc <= 4mhz for PCM/PCH , 3mhz to 10 mhz for PCD
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NODEBUG                  //No Debug mode for ICD
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES NOWRT                    //Program memory not write protected
#FUSES RESERVED                 //Used to set the reserved FUSE bits

#use delay(clock=4000000)
#use rs232(baud=9600,parity=N,xmit=PIN_C6,rcv=PIN_C7,bits=8)

void main()
{

   setup_adc_ports(ALL_ANALOG);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_psp(PSP_DISABLED);
   setup_spi(SPI_SS_DISABLED);
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DIV_BY_16, 255,1);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);
   set_adc_channel(0);

   float valorLido;
   float temperatura; 
   float tempMax;
   float tempMim;  
   int cont=0;

    if (read_eeprom(2) == 0xFF && read_eeprom(3) == 0xFF)   
    {
      tempMim=255;
      write_eeprom(2,tempMim);
      tempMax=0;
      write_eeprom(3,tempMax);
    }
    else  
    {
      tempMim = (read_eeprom(2));
      delay_ms(100);
      tempMax = (read_eeprom(3)); 
      delay_ms(100);
    }

   while(true)
   {  
     
      valorLido=read_adc();
      temperatura=valorLido*0.00488/0.01+0.5;

      if (temperatura < tempMim)
      {
        tempMim = temperatura;
      }

      if (temperatura > tempMax)
      {
        tempMax = temperatura;
      }

      cont ++;

      printf("\n Temperatura Atual: %f\r",temperatura);
      delay_ms(2000);
      printf("\n Temperatura Maxima: %f\r",tempMax);
      delay_ms(2000);
      printf("\n Temperatura Minima: %f\r",tempMim);
      delay_ms(2000);
      printf("\n Leitura Numero: %d \r",cont);
      delay_ms(2000);
      printf("\n  \r");
      
      }

}
