#include <16F877A.h>

#device adc=10
#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                       //Crystal osc <= 4mhz for PCM/PCH , 3mhz to 10 mhz for PCD
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NODEBUG                  //No Debug mode for ICD
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES NOWRT                    //Program memory not write protected
#FUSES RESERVED                 //Used to set the reserved FUSE bits
#use delay(clock=4000000)

#include <LCDNew.c>

void main()
{
   
   setup_adc(ADC_CLOCK_INTERNAL);
   setup_adc_ports(ALL_ANALOG);
   setup_spi(SPI_SS_DISABLED);    
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DIV_BY_16, 255,1);
   setup_ccp1(CCP_PWM);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE); 
   set_adc_channel(0);

   float valorLido;
   float temperatura; 
   float tempMax;
   float tempMim;  
   int i,j=16;
   float tempMim1;
   float tempMim2;
   int tempMax1;

   lcd_init(); 

  for (i = 0; i < 16; ++i)
  {
    --j;
    lcd_gotoxy(i,1);
    printf(lcd_putc, "*");
    lcd_gotoxy(j,0);
    printf(lcd_putc, "*");
    delay_ms(300);
  }
   
    lcd_gotoxy(4,1);
    delay_ms(500);
    printf(lcd_putc, "Bem Vindo");  
    delay_ms(2000);

    if (read_eeprom(2) == 0xFF && read_eeprom(3) == 0xFF)   
    {
      tempMim=255;
      write_eeprom(2,tempMim);
      tempMax=0;
      write_eeprom(3,tempMax);

      tempMim1=255;
      write_eeprom(4,tempMim1);
      tempMim2=255;
      write_eeprom(5,tempMim2);
      tempMax=0;
      write_eeprom(6,tempMax1);
    }
    else  
    {
      tempMim = (read_eeprom(2));
      delay_ms(100);
      tempMax = (read_eeprom(3)); 
      delay_ms(100);

       //***************************
      tempMim1 = (read_eeprom(4));
      delay_ms(100);
      tempMim2 = (read_eeprom(5));
      delay_ms(100);
      tempMax1 = (read_eeprom(6)); 
      delay_ms(100);
       //***************************
    }

   while(true)
   {  
     
      valorLido=read_adc();
      temperatura=valorLido*0.00488/0.01+0.5;

      if (temperatura < tempMim)
      {
        tempMim = temperatura;
      }

      if (temperatura > tempMax)
      {
        tempMax = temperatura;
      }



      //***************************

       if (temperatura < tempMim1)
      {
        tempMim1 = temperatura;
      }
       if (temperatura < tempMim2)
      {
        tempMim2 = temperatura;
      }
      if (temperatura > tempMax1)
      {
        tempMax1 = temperatura;
      }
      //+++++++++++++++++++++++++++

      printf(lcd_putc, "\f Temp: %f",temperatura); 
      delay_ms(3000); 
      printf(lcd_putc, "\f Max: %f",tempMax);
      delay_ms(3000); 
      printf(lcd_putc, "\f Mim: %f",tempMim);
      delay_ms(3000); 

       //***************************
      printf(lcd_putc, "\f Mim1: %f",tempMim1);
      delay_ms(3000); 
      printf(lcd_putc, "\f Mim2: %f",tempMim2);
      delay_ms(3000); 
      printf(lcd_putc, "\f Max1: %u",tempMax1);
      delay_ms(3000);
      //***************************

      if (input(pin_d3) == 0)
      {     
        write_eeprom(2,255);
        write_eeprom(3,0);
        tempMim=255;
        tempMax=0;

         write_eeprom(4,255);
         write_eeprom(5,255);
         write_eeprom(6,0);
      }

      write_eeprom(2,tempMim);
      delay_ms(100);
      write_eeprom(3,tempMax);
      delay_ms(100);

      write_eeprom(4,tempMim1);
      delay_ms(100);
      write_eeprom(5,tempMim2);
      delay_ms(100);
      write_eeprom(6,tempMax1);
      delay_ms(100);
    

      

      
   }

}
