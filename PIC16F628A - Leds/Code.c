#include <16F628A.h>
#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                    //External clock
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOBROWNOUT               //No brownout reset
#FUSES MCLR                     //Master Clear pin enabled
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection

#use delay(clock=4000000)

                          

void main()
{
                               
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_ccp1(CCP_OFF);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

   
while(true)

{

   Output_high(pin_b0);
   delay_ms(200);
   Output_low(pin_b0);

   Output_high(pin_b1);
   delay_ms(200);
   Output_low(pin_b1);

   Output_high(pin_b2);
   delay_ms(200);
   Output_low(pin_b2);

   Output_high(pin_b3);
   delay_ms(200);
   Output_low(pin_b3);

}









}
