#include <16F628A.h>
#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                    //External clock
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOBROWNOUT               //No brownout reset
#FUSES MCLR                     //Master Clear pin enabled
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#use delay(clock=4000000)
                        
void main()
{
                               
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_ccp1(CCP_OFF);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

int cont;   
while(true)

{
   if (cont>9)
   {
      cont=0;
   }
   cont++;

      switch(cont)
      {
      case 0:
      Output_low(pin_a0);
      Output_low(pin_a1);
      Output_low(pin_a2);
      Output_low(pin_a3);

      case 1:
      Output_low(pin_a0);
      Output_low(pin_a1);
      Output_low(pin_a2);
      Output_high(pin_a3);

      case 2:
      Output_low(pin_a0);
      Output_low(pin_a1);
      Output_high(pin_a2);
      Output_low(pin_a3);

      case 3:
      Output_low(pin_a0);
      Output_low(pin_a1);
      Output_high(pin_a2);
      Output_high(pin_a3);

      case 4:
      Output_low(pin_a0);
      Output_high(pin_a1);
      Output_low(pin_a2);
      Output_low(pin_a3);

      case 5:
      Output_low(pin_a0);
      Output_high(pin_a1);
      Output_low(pin_a2);
      Output_high(pin_a3);

      case 6:
      Output_high(pin_a0);
      Output_low(pin_a1);
      Output_high(pin_a2);
      Output_low(pin_a3);

      case 7:
      Output_low(pin_a0);
      Output_high(pin_a1);
      Output_high(pin_a2);
      Output_high(pin_a3);

      case 8:
      Output_high(pin_a0);
      Output_low(pin_a1);
      Output_low(pin_a2);
      Output_low(pin_a3);

      case 9:
      Output_high(pin_a0);
      Output_low(pin_a1);
      Output_low(pin_a2);
      Output_high(pin_a3);

      }
  
}



