#include "C:\Users\Vitor\Desktop\Tutorial - 15 - Sensor de Corrente\Code.h"


void main()
{

   setup_adc_ports(AN0_TO_AN1|VSS_VDD);
   setup_adc(ADC_CLOCK_INTERNAL);
   setup_spi(SPI_SS_DISABLED);
   setup_wdt(WDT_OFF);
   setup_timer_0(RTCC_INTERNAL);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

   while (true){

      float v;

      output_high(pin_c0);
      delay_ms(1000);
      set_adc_channel(0);
      delay_us(10);
      v=read_adc();
      v=v*0.0048875855327468;

      printf("%f \n \r",v );

   }

}
