#include "C:\Users\Vitor\Desktop\Tutorial - 16 - RS232 01\Code.h"    


void main()
{

   setup_adc_ports(AN0_TO_AN1|VSS_VDD);
   setup_adc(ADC_CLOCK_INTERNAL);
   setup_spi(SPI_SS_DISABLED);
   setup_wdt(WDT_OFF);
   setup_timer_0(RTCC_INTERNAL);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_timer_3(T3_DISABLED|T3_DIV_BY_1);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

   while(true){
    
      int i;
      int x[10];

      x[0]="V";
      x[1]="i";
      x[2]="t";
      x[3]="o";
      x[4]="r";
      x[5]="I";
      x[6]="v";
      x[7]="a";
      x[8]="n";
      x[9]="!";

      for (i = 0; i < 10; ++i)
      {
         delay_ms(100);
         putc(x[i]);
         
         while (x[i] == '!')
         {
            printf("\n \r");
            delay_ms(500);
            x[i] = x[i+1];
         }
      }




   }

}
