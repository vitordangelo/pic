#include <16F877A.h>
#device adc=10
#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                       //Crystal osc <= 4mhz for PCM/PCH , 3mhz to 10 mhz for PCD
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NODEBUG                  //No Debug mode for ICD
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES NOWRT                    //Program memory not write protected
#FUSES RESERVED                 //Used to set the reserved FUSE bits
#use delay(clock=4000000)

void main()
{
   
   setup_adc(ADC_CLOCK_INTERNAL);
   setup_adc_ports(ALL_ANALOG);
   setup_spi(SPI_SS_DISABLED);    
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DIV_BY_16, 255,1);
   setup_ccp1(CCP_PWM);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE); 
   set_adc_channel(0);

   float valorLido;
   float temperatura;  

   #use fast_io(b)
   #use fast_io(c)
   #use fast_io(d)

   set_tris_b(0b00000000);
   set_tris_c(0b00000000);
   set_tris_d(0b00000000);
 
   while(true)
   {  
      valorLido=read_adc();
      temperatura=valorLido*0.0048/0.01+0.5;

      if (temperatura>=15 && temperatura <16)      
        {
           output_d(0b00001100);
           output_c(0b11011010);
        }  

      if (temperatura>=16 && temperatura <17)      
        {
           output_b(0b00001100);
           output_c(0b11111000);
        } 

        if (temperatura>=17 && temperatura <18)      
        {
           output_b(0b00001100);
           output_c(0b01001110);
        }

        if (temperatura>=18 && temperatura <19)      
        {
           output_b(0b00001100);
           output_c(0b11111110);
        }

        if (temperatura>=19 && temperatura <20)      
        {
           output_b(0b00001100);
           output_c(0b11001110); 
        }

        if (temperatura>=20 && temperatura <21)      
        {
           output_b(0b10110110);
           output_c(0b01111110);
        }

        if (temperatura>=21 && temperatura <22)      
        {
           output_b(0b10110110);
           output_c(0b00001100);
        }

        if (temperatura>=22 && temperatura <23)      
        {
           output_b(0b10110110);
           output_c(0b10110110);
        }

        if (temperatura>=23 && temperatura <24)      
        {
           output_b(0b10110110);
           output_c(0b10011110);
        }

        if (temperatura>=24 && temperatura <25)      
        {
           output_b(0b10110110);
           output_c(0b11001100);
        }

        if (temperatura>=25 && temperatura <26)      
        {
           output_b(0b10110110);
           output_c(0b11011010);
        }

        if (temperatura>=26 && temperatura <27)      
        {
           output_b(0b10110110);
           output_c(0b11111000);
        }

        if (temperatura>=27 && temperatura <28)      
        {
           output_b(0b10110110);
           output_c(0b01001110);
        }

        if (temperatura>=28 && temperatura <29)      
        {
           output_b(0b10110110);
           output_c(0b11111110);
        }

        if (temperatura>=29 && temperatura <30)      
        {
           output_b(0b10110110);
           output_c(0b11001110);
        }

        if (temperatura>=30 && temperatura <31)      
        {
           output_b(0b10011110);
           output_c(0b01111110);
        }

        if (temperatura>=31 && temperatura <32)      
        {
           output_b(0b10011110);
           output_c(0b00001100);
        }

        if (temperatura>=32 && temperatura <33)      
        {
           output_b(0b10011110);
           output_c(0b10110110);
        }

        if (temperatura>=33 && temperatura <34)      
        {
           output_b(0b10011110);
           output_c(0b10011110);
        }

        if (temperatura>=34 && temperatura <35)      
        {
           output_b(0b10011110);
           output_c(0b11001100);
        }
   }

}
