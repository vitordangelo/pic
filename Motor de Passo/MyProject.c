#define BOBINA0 PORTD.RD0
#define BOBINA1 PORTD.RD1
#define BOBINA2 PORTD.RD2
#define BOBINA3 PORTD.RD3
#define BT1 PORTD.RD4
#define BT2 PORTD.RD5

int const LIG=1, ON=1, OFF=0;

void main()
{
	while(1)
	{

		TRISC=0xFF;
		TRISD=0;

			BOBINA3=OFF;
			BOBINA2=OFF;
			BOBINA1=OFF;
			BOBINA0=OFF;

		while (BT1 == 0)
		{

			BOBINA3=ON;
			BOBINA2=OFF;
			BOBINA1=OFF;
			BOBINA0=OFF;
			delay_ms(2000);

			BOBINA3=ON;
			BOBINA2=ON;
			BOBINA1=OFF;
			BOBINA0=OFF;
			delay_ms(2000);

			BOBINA3=OFF;
			BOBINA2=ON;
			BOBINA1=OFF;
			BOBINA0=OFF;
			delay_ms(2000);

			BOBINA3=OFF;
			BOBINA2=ON;
			BOBINA1=ON;
			BOBINA0=OFF;
			delay_ms(2000);

			BOBINA3=OFF;
			BOBINA2=OFF;
			BOBINA1=ON;
			BOBINA0=OFF;
			delay_ms(2000);

			BOBINA3=OFF;
			BOBINA2=OFF;
			BOBINA1=ON;
			BOBINA0=ON;
			delay_ms(2000);

			BOBINA3=OFF;
			BOBINA2=OFF;
			BOBINA1=OFF;
			BOBINA0=ON;
			delay_ms(2000);

			BOBINA3=ON;
			BOBINA2=OFF;
			BOBINA1=OFF;
			BOBINA0=ON;
			delay_ms(2000);
		}

		while(BT2 == 0)
		{

			BOBINA3=ON;
			BOBINA2=OFF;
			BOBINA1=OFF;
			BOBINA0=ON;
			delay_ms(2000);

			BOBINA3=OFF;
			BOBINA2=OFF;
			BOBINA1=OFF;
			BOBINA0=ON;
			delay_ms(2000);

			BOBINA3=OFF;
			BOBINA2=OFF;
			BOBINA1=ON;
			BOBINA0=ON;
			delay_ms(2000);

			BOBINA3=OFF;
			BOBINA2=OFF;
			BOBINA1=ON;
			BOBINA0=OFF;
			delay_ms(2000);

			BOBINA3=OFF;
			BOBINA2=ON;
			BOBINA1=ON;
			BOBINA0=OFF;
			delay_ms(2000);

			BOBINA3=OFF;
			BOBINA2=ON;
			BOBINA1=OFF;
			BOBINA0=OFF;
			delay_ms(2000);

			BOBINA3=ON;
			BOBINA2=ON;
			BOBINA1=OFF;
			BOBINA0=OFF;
			delay_ms(2000);

			BOBINA3=ON;
			BOBINA2=OFF;
			BOBINA1=OFF;
			BOBINA0=OFF;
			delay_ms(2000);

		}



	}
}











