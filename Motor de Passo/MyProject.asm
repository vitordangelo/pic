
_main:

;MyProject.c,10 :: 		void main()
;MyProject.c,12 :: 		while(1)
L_main0:
;MyProject.c,15 :: 		TRISC=0xFF;
	MOVLW      255
	MOVWF      TRISC+0
;MyProject.c,16 :: 		TRISD=0;
	CLRF       TRISD+0
;MyProject.c,18 :: 		BOBINA3=OFF;
	BCF        PORTD+0, 3
;MyProject.c,19 :: 		BOBINA2=OFF;
	BCF        PORTD+0, 2
;MyProject.c,20 :: 		BOBINA1=OFF;
	BCF        PORTD+0, 1
;MyProject.c,21 :: 		BOBINA0=OFF;
	BCF        PORTD+0, 0
;MyProject.c,23 :: 		while (BT1 == 0)
L_main2:
	BTFSC      PORTD+0, 4
	GOTO       L_main3
;MyProject.c,26 :: 		BOBINA3=ON;
	BSF        PORTD+0, 3
;MyProject.c,27 :: 		BOBINA2=OFF;
	BCF        PORTD+0, 2
;MyProject.c,28 :: 		BOBINA1=OFF;
	BCF        PORTD+0, 1
;MyProject.c,29 :: 		BOBINA0=OFF;
	BCF        PORTD+0, 0
;MyProject.c,30 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;MyProject.c,32 :: 		BOBINA3=ON;
	BSF        PORTD+0, 3
;MyProject.c,33 :: 		BOBINA2=ON;
	BSF        PORTD+0, 2
;MyProject.c,34 :: 		BOBINA1=OFF;
	BCF        PORTD+0, 1
;MyProject.c,35 :: 		BOBINA0=OFF;
	BCF        PORTD+0, 0
;MyProject.c,36 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
	NOP
;MyProject.c,38 :: 		BOBINA3=OFF;
	BCF        PORTD+0, 3
;MyProject.c,39 :: 		BOBINA2=ON;
	BSF        PORTD+0, 2
;MyProject.c,40 :: 		BOBINA1=OFF;
	BCF        PORTD+0, 1
;MyProject.c,41 :: 		BOBINA0=OFF;
	BCF        PORTD+0, 0
;MyProject.c,42 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main6:
	DECFSZ     R13+0, 1
	GOTO       L_main6
	DECFSZ     R12+0, 1
	GOTO       L_main6
	DECFSZ     R11+0, 1
	GOTO       L_main6
	NOP
	NOP
;MyProject.c,44 :: 		BOBINA3=OFF;
	BCF        PORTD+0, 3
;MyProject.c,45 :: 		BOBINA2=ON;
	BSF        PORTD+0, 2
;MyProject.c,46 :: 		BOBINA1=ON;
	BSF        PORTD+0, 1
;MyProject.c,47 :: 		BOBINA0=OFF;
	BCF        PORTD+0, 0
;MyProject.c,48 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main7:
	DECFSZ     R13+0, 1
	GOTO       L_main7
	DECFSZ     R12+0, 1
	GOTO       L_main7
	DECFSZ     R11+0, 1
	GOTO       L_main7
	NOP
	NOP
;MyProject.c,50 :: 		BOBINA3=OFF;
	BCF        PORTD+0, 3
;MyProject.c,51 :: 		BOBINA2=OFF;
	BCF        PORTD+0, 2
;MyProject.c,52 :: 		BOBINA1=ON;
	BSF        PORTD+0, 1
;MyProject.c,53 :: 		BOBINA0=OFF;
	BCF        PORTD+0, 0
;MyProject.c,54 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main8:
	DECFSZ     R13+0, 1
	GOTO       L_main8
	DECFSZ     R12+0, 1
	GOTO       L_main8
	DECFSZ     R11+0, 1
	GOTO       L_main8
	NOP
	NOP
;MyProject.c,56 :: 		BOBINA3=OFF;
	BCF        PORTD+0, 3
;MyProject.c,57 :: 		BOBINA2=OFF;
	BCF        PORTD+0, 2
;MyProject.c,58 :: 		BOBINA1=ON;
	BSF        PORTD+0, 1
;MyProject.c,59 :: 		BOBINA0=ON;
	BSF        PORTD+0, 0
;MyProject.c,60 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main9:
	DECFSZ     R13+0, 1
	GOTO       L_main9
	DECFSZ     R12+0, 1
	GOTO       L_main9
	DECFSZ     R11+0, 1
	GOTO       L_main9
	NOP
	NOP
;MyProject.c,62 :: 		BOBINA3=OFF;
	BCF        PORTD+0, 3
;MyProject.c,63 :: 		BOBINA2=OFF;
	BCF        PORTD+0, 2
;MyProject.c,64 :: 		BOBINA1=OFF;
	BCF        PORTD+0, 1
;MyProject.c,65 :: 		BOBINA0=ON;
	BSF        PORTD+0, 0
;MyProject.c,66 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main10:
	DECFSZ     R13+0, 1
	GOTO       L_main10
	DECFSZ     R12+0, 1
	GOTO       L_main10
	DECFSZ     R11+0, 1
	GOTO       L_main10
	NOP
	NOP
;MyProject.c,68 :: 		BOBINA3=ON;
	BSF        PORTD+0, 3
;MyProject.c,69 :: 		BOBINA2=OFF;
	BCF        PORTD+0, 2
;MyProject.c,70 :: 		BOBINA1=OFF;
	BCF        PORTD+0, 1
;MyProject.c,71 :: 		BOBINA0=ON;
	BSF        PORTD+0, 0
;MyProject.c,72 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main11:
	DECFSZ     R13+0, 1
	GOTO       L_main11
	DECFSZ     R12+0, 1
	GOTO       L_main11
	DECFSZ     R11+0, 1
	GOTO       L_main11
	NOP
	NOP
;MyProject.c,73 :: 		}
	GOTO       L_main2
L_main3:
;MyProject.c,75 :: 		while(BT2 == 0)
L_main12:
	BTFSC      PORTD+0, 5
	GOTO       L_main13
;MyProject.c,78 :: 		BOBINA3=ON;
	BSF        PORTD+0, 3
;MyProject.c,79 :: 		BOBINA2=OFF;
	BCF        PORTD+0, 2
;MyProject.c,80 :: 		BOBINA1=OFF;
	BCF        PORTD+0, 1
;MyProject.c,81 :: 		BOBINA0=ON;
	BSF        PORTD+0, 0
;MyProject.c,82 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main14:
	DECFSZ     R13+0, 1
	GOTO       L_main14
	DECFSZ     R12+0, 1
	GOTO       L_main14
	DECFSZ     R11+0, 1
	GOTO       L_main14
	NOP
	NOP
;MyProject.c,84 :: 		BOBINA3=OFF;
	BCF        PORTD+0, 3
;MyProject.c,85 :: 		BOBINA2=OFF;
	BCF        PORTD+0, 2
;MyProject.c,86 :: 		BOBINA1=OFF;
	BCF        PORTD+0, 1
;MyProject.c,87 :: 		BOBINA0=ON;
	BSF        PORTD+0, 0
;MyProject.c,88 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main15:
	DECFSZ     R13+0, 1
	GOTO       L_main15
	DECFSZ     R12+0, 1
	GOTO       L_main15
	DECFSZ     R11+0, 1
	GOTO       L_main15
	NOP
	NOP
;MyProject.c,90 :: 		BOBINA3=OFF;
	BCF        PORTD+0, 3
;MyProject.c,91 :: 		BOBINA2=OFF;
	BCF        PORTD+0, 2
;MyProject.c,92 :: 		BOBINA1=ON;
	BSF        PORTD+0, 1
;MyProject.c,93 :: 		BOBINA0=ON;
	BSF        PORTD+0, 0
;MyProject.c,94 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main16:
	DECFSZ     R13+0, 1
	GOTO       L_main16
	DECFSZ     R12+0, 1
	GOTO       L_main16
	DECFSZ     R11+0, 1
	GOTO       L_main16
	NOP
	NOP
;MyProject.c,96 :: 		BOBINA3=OFF;
	BCF        PORTD+0, 3
;MyProject.c,97 :: 		BOBINA2=OFF;
	BCF        PORTD+0, 2
;MyProject.c,98 :: 		BOBINA1=ON;
	BSF        PORTD+0, 1
;MyProject.c,99 :: 		BOBINA0=OFF;
	BCF        PORTD+0, 0
;MyProject.c,100 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main17:
	DECFSZ     R13+0, 1
	GOTO       L_main17
	DECFSZ     R12+0, 1
	GOTO       L_main17
	DECFSZ     R11+0, 1
	GOTO       L_main17
	NOP
	NOP
;MyProject.c,102 :: 		BOBINA3=OFF;
	BCF        PORTD+0, 3
;MyProject.c,103 :: 		BOBINA2=ON;
	BSF        PORTD+0, 2
;MyProject.c,104 :: 		BOBINA1=ON;
	BSF        PORTD+0, 1
;MyProject.c,105 :: 		BOBINA0=OFF;
	BCF        PORTD+0, 0
;MyProject.c,106 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main18:
	DECFSZ     R13+0, 1
	GOTO       L_main18
	DECFSZ     R12+0, 1
	GOTO       L_main18
	DECFSZ     R11+0, 1
	GOTO       L_main18
	NOP
	NOP
;MyProject.c,108 :: 		BOBINA3=OFF;
	BCF        PORTD+0, 3
;MyProject.c,109 :: 		BOBINA2=ON;
	BSF        PORTD+0, 2
;MyProject.c,110 :: 		BOBINA1=OFF;
	BCF        PORTD+0, 1
;MyProject.c,111 :: 		BOBINA0=OFF;
	BCF        PORTD+0, 0
;MyProject.c,112 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main19:
	DECFSZ     R13+0, 1
	GOTO       L_main19
	DECFSZ     R12+0, 1
	GOTO       L_main19
	DECFSZ     R11+0, 1
	GOTO       L_main19
	NOP
	NOP
;MyProject.c,114 :: 		BOBINA3=ON;
	BSF        PORTD+0, 3
;MyProject.c,115 :: 		BOBINA2=ON;
	BSF        PORTD+0, 2
;MyProject.c,116 :: 		BOBINA1=OFF;
	BCF        PORTD+0, 1
;MyProject.c,117 :: 		BOBINA0=OFF;
	BCF        PORTD+0, 0
;MyProject.c,118 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main20:
	DECFSZ     R13+0, 1
	GOTO       L_main20
	DECFSZ     R12+0, 1
	GOTO       L_main20
	DECFSZ     R11+0, 1
	GOTO       L_main20
	NOP
	NOP
;MyProject.c,120 :: 		BOBINA3=ON;
	BSF        PORTD+0, 3
;MyProject.c,121 :: 		BOBINA2=OFF;
	BCF        PORTD+0, 2
;MyProject.c,122 :: 		BOBINA1=OFF;
	BCF        PORTD+0, 1
;MyProject.c,123 :: 		BOBINA0=OFF;
	BCF        PORTD+0, 0
;MyProject.c,124 :: 		delay_ms(2000);
	MOVLW      11
	MOVWF      R11+0
	MOVLW      38
	MOVWF      R12+0
	MOVLW      93
	MOVWF      R13+0
L_main21:
	DECFSZ     R13+0, 1
	GOTO       L_main21
	DECFSZ     R12+0, 1
	GOTO       L_main21
	DECFSZ     R11+0, 1
	GOTO       L_main21
	NOP
	NOP
;MyProject.c,126 :: 		}
	GOTO       L_main12
L_main13:
;MyProject.c,130 :: 		}
	GOTO       L_main0
;MyProject.c,131 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
