#include <16F628A.h>

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                       //Crystal osc <= 4mhz for PCM/PCH , 3mhz to 10 mhz for PCD
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOBROWNOUT               //No brownout reset
#FUSES MCLR                     //Master Clear pin enabled
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES RESERVED                 //Used to set the reserved FUSE bits
#use delay(clock=4000000)

void main()
{

   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DIV_BY_16,207,1);
   setup_ccp1(CCP_PWM);
   set_pwm1_duty(1023);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

 while(true)
{
      int i;
      for (i = 0; i < 255; ++i)
      {
         set_pwm1_duty(i);
         delay_ms(20);
      }


}
}
