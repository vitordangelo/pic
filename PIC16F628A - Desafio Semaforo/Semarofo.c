#include <16F628A.h>

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                       //Crystal osc <= 4mhz for PCM/PCH , 3mhz to 10 mhz for PCD
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOBROWNOUT               //No brownout reset
#FUSES MCLR                     //Master Clear pin enabled
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES RESERVED                 //Used to set the reserved FUSE bits

#use delay(clock=4000000)

#use fast_io(b)
#use fast_io(a)

int estado=0;

#INT_EXT
 void teste(void)
   {
      while(input(pin_b0) == 0)
      {
        output_a(0b00000000);
        if (estado ==10)
        {
          output_b(0b0000100); // Amarelo
          delay_ms(500);
          output_b(0b0001000); //Vermelho
          delay_ms(5000);
        }

        if (estado ==20)
        {
          
        }

        if (estado ==30)
        {
          output_b(0b0001000); //Vermelho
          delay_ms(5000); 
        }
        

      }
   }

void main()
{
    set_tris_a(0b00000000);
    set_tris_b(0b00000001);

    ext_int_edge(0,H_TO_L);
    enable_interrupts(global);
    enable_interrupts(INT_EXT);

   while(true)
   {
      estado=10;
      output_b(0b0000010); // Verde 
      delay_ms(3000);

      estado=20;
      output_b(0b0000100); // Amarelo
      delay_ms(1000);

      estado=30;
      output_b(0b0001000); //Vermelho
      delay_ms(5000);
   }

}
