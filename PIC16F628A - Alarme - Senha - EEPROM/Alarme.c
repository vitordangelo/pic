#include <16F628A.h>

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                       //Crystal osc <= 4mhz for PCM/PCH , 3mhz to 10 mhz for PCD
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOBROWNOUT               //No brownout reset
#FUSES MCLR                     //Master Clear pin enabled
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES RESERVED                 //Used to set the reserved FUSE bits

#use delay(clock=4000000)

boolean ativado;

void Armado(void) //Senha Incorreta
{
     int i;
   
   for(i=0; i<30; i++)
   {
      Output_high(pin_b6);
      delay_ms(100);
      Output_low(pin_b6);
      delay_ms(100);   
   }
   Output_high(pin_b5); //Led indica se alarme esta ativado
   ativado=true;
}
void Desarmado(void) //Senha Correta
{
     int i;
   
   for(i=0; i<30; i++)
   {
      Output_high(pin_b7);
      delay_ms(100);
      Output_low(pin_b7);
      delay_ms(100);   
   }
   Output_low(pin_b5); //Led indica se alarme esta ativado
   ativado=false;
   Output_low(pin_b3);
}

 int LerBotoes(void)
{
   if(input(pin_a0) == 0)
      return 0;
   
   if(input(pin_a1) == 0)
      return 1;
      
   if(input(pin_a2) == 0)
      return 2;
      
   if(input(pin_a3) == 0)
      return 3;

   return 0xFF;
}

/* void teste(void)
   {
     if (input(pin_b0) == 0 && ativado==true)
     {
         Output_high(pin_b3);
         delay_ms(5000);
         Output_low(pin_b3);
     }
   }*/


void main (void)
{
/* setup_timer_0(RTCC_EXT_H_TO_L|RTCC_DIV_64);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_ccp1(CCP_OFF);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);
   enable_interrupts(INT_RTCC);
   enable_interrupts(GLOBAL);*/

/*  ext_int_edge(0,H_TO_L);
    enable_interrupts(global);
    enable_interrupts(INT_EXT);*/

    int i,j;
    int senha0,senha1,senha2,senha3;
    int digito0,digito1,digito2,digito3;

   if(read_eeprom(0) == 0xFF && read_eeprom(1) == 0xFF && read_eeprom(2) == 0xFF && read_eeprom(3) == 0xFF) //Senha pré definida, memória LIMPA.
   {
      write_eeprom(0,0);
      write_eeprom(1,1);
      write_eeprom(2,2);
      write_eeprom(3,3);
   }

   else
   {
      senha0=read_eeprom(0);     
      senha1=read_eeprom(1);  
      senha2=read_eeprom(2);  
      senha3=read_eeprom(3);  
   }

   while(true)
   {
      
      do //Leitura do digito 0
      {
         i = LerBotoes();
      }  
      while(i == 0xFF);
      
      delay_ms(300);
      digito0 = i;

      do //Leitura do digito 1
      {
         i = LerBotoes();
      }
      while(i == 0xFF);
      
      delay_ms(300);
      digito1 = i;

      do //Leitura do digito 2
      {
         i = LerBotoes();
      }
      while(i == 0xFF);
      
      delay_ms(300);
      digito2 = i;

      do //Leitura do Digito 3
      {
         i = LerBotoes();
      }
      while(i == 0xFF);
      
      delay_ms(300);
      digito3 = i;


       if(input(pin_a4) == 0)//Grava nova senha
      {
         senha0=digito0;
         write_eeprom(0,digito0);
         senha1=digito1;
         write_eeprom(1,digito1);
         senha2=digito2;
         write_eeprom(2,digito2);
         senha3=digito3;
         write_eeprom(3,digito3);  
      }

      j = 0; //Verifica a senha
      if (senha0==digito0)
      {
         j++;
      }
       if (senha1==digito1)
      {
         j++;
      }
       if (senha1==digito1)
      {
         j++;
      }
       if (senha1==digito1)
      {
         j++;
      }
      
      
      if(j == 4)        //se senha correta
      {
        Desarmado();
      }
      else              //se senha errada
      {
         Armado();
      }

      if (ativado==true)
      {
        Output_high(pin_b0);
      }
      else Output_low(pin_b0);

      
      





     

   }
}