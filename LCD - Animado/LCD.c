#include <16F877A.h>
#device adc=8
#FUSES NOWDT                    //No Watch Dog Timer
#FUSES HS                       //High speed Osc (> 4mhz for PCM/PCH) (>10mhz for PCD)
#FUSES NOPUT                    //No Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NODEBUG                  //No Debug mode for ICD
#FUSES NOBROWNOUT               //No brownout reset
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES NOWRT                    //Program memory not write protected
#use delay(clock=4000000)

#include <_LCD.c>

int temperatura=0;
int valorLido=0;

void main()
{

   setup_adc(ADC_CLOCK_INTERNAL);
   setup_adc_ports(ALL_ANALOG);
   setup_spi(SPI_SS_DISABLED);
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DIV_BY_16, 255,1);
   setup_ccp1(CCP_PWM);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE); 
   set_adc_channel(0);
   
   lcd_init();                                                     

   while(true)
   {

      lcd_gotoxy(3,8); 
      lcd_putc("\f12345678");
      delay_ms(200);
              





      /*Output_high(pin_b1);

      printf(lcd_putc, "\f Bem Vindo");  
      delay_ms(1000);

      valorLido=read_adc();
      temperatura=valorLido*2;

      while(temperatura < 20)
      {
         valorLido=read_adc();
         temperatura=valorLido*2;
   
         printf(lcd_putc, "\f Temp:  %u",temperatura);  
        
         Output_high(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(300);

         Output_high(pin_c4);
         Output_high(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(300);

         Output_low(pin_c4);
         Output_high(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(300);

         Output_low(pin_c4);
         Output_high(pin_c3);
         Output_high(pin_c1);
         Output_low(pin_c0);
         delay_ms(300);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_high(pin_c1);
         Output_low(pin_c0);
         delay_ms(300);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_high(pin_c1);
         Output_high(pin_c0);
         delay_ms(300);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_high(pin_c0);
         delay_ms(300);

         Output_high(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_high(pin_c0);
         delay_ms(300);

         set_pwm1_duty(255);
       
      }
      while(temperatura > 20)
      {
         valorLido=read_adc();
         temperatura=valorLido*2;
   
         printf(lcd_putc, "\f Temp:  %u",temperatura);  
        
         Output_high(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(50);

         Output_high(pin_c4);
         Output_high(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(50);

         Output_low(pin_c4);
         Output_high(pin_c3);
         Output_low(pin_c1);
         Output_low(pin_c0);
         delay_ms(50);

         Output_low(pin_c4);
         Output_high(pin_c3);
         Output_high(pin_c1);
         Output_low(pin_c0);
         delay_ms(50);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_high(pin_c1);
         Output_low(pin_c0);
         delay_ms(50);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_high(pin_c1);
         Output_high(pin_c0);
         delay_ms(50);

         Output_low(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_high(pin_c0);
         delay_ms(50);

         Output_high(pin_c4);
         Output_low(pin_c3);
         Output_low(pin_c1);
         Output_high(pin_c0);
         delay_ms(50);  

         set_pwm1_duty(127);
      }



      

*/
                          
   }

}        
