#include <16F628A.h>

#FUSES NOWDT                    //No Watch Dog Timer
#FUSES XT                       //Crystal osc <= 4mhz for PCM/PCH , 3mhz to 10 mhz for PCD
#FUSES PUT                      //Power Up Timer
#FUSES NOPROTECT                //Code not protected from reading
#FUSES NOBROWNOUT               //No brownout reset
#FUSES MCLR                     //Master Clear pin enabled
#FUSES NOLVP                    //No low voltage prgming, B3(PIC16) or B5(PIC18) used for I/O
#FUSES NOCPD                    //No EE protection
#FUSES RESERVED                 //Used to set the reserved FUSE bits

#use delay(clock=4000000)

#use fast_io(b)
//#use fast_io(a)

const unsigned char BCD_7SEG[10] =
{
   //gfedcba-
   0b01111110, //0
   0b00001100, //1
   0b10110110, //2
   0b10011110, //3
   0b11001100, //4
   0b11011010, //5
   0b11111000, //6
   0b01001110, //7
   0b11111110, //8
   0b11001110  //9
};

unsigned char cont = 0;

#INT_EXT
void IncrementaDisplay (void)
{
   delay_ms(500);   
   cont ++;
   if (cont>9)
   {
      cont=0;
   }      
}

void main()
{
   //set_tris_a(0b00000000);
   set_tris_b(0b00000001);

    ext_int_edge(0,H_TO_L);
    enable_interrupts(global);
    enable_interrupts(INT_EXT);

    while (true)
    {
       output_b(BCD_7SEG[cont]);
    }
}
