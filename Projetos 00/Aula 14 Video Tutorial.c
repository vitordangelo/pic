#include "H:\Eletrônica\PIC\Aula 14 Video Tutorial.h"  

float v;

void main()
{

   setup_adc_ports(AN0_TO_AN1|VSS_VDD);
   setup_adc(ADC_CLOCK_INTERNAL);
   setup_spi(SPI_SS_DISABLED);
   setup_wdt(WDT_OFF);
   setup_timer_0(RTCC_INTERNAL);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_timer_3(T3_DISABLED|T3_DIV_BY_1);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

   while(true){
      output_high(pin_c0);  
      delay_ms(300);  
                              
      set_adc_channel(1);
      delay_us(10); 
      v=read_adc();
      v=v*0.0048875855327468;
      
      if (v==2.29)
      {
           output_high(pin_b3); 
           delay_ms(500);
           output_low(pin_b3);
           delay_ms(500);
      }

      if ((v<1.65)&&(v>1.55))
      {
         output_high(pin_b4);  
      }

      delay_ms(300);

      output_low(pin_b3); 
      output_low(pin_b4);


      printf("%f  \n \r",v );
      


   }

}






