#include <16F628A.h>

#FUSES NOWDT                 	//No Watch Dog Timer
#FUSES INTRC                 	//Internal RC Osc
#FUSES NOPUT                 	//No Power Up Timer
#FUSES NOPROTECT             	//Code not protected from reading
#FUSES BROWNOUT              	//Reset when brownout detected
#FUSES NOMCLR                	//Master Clear pin used for I/O
#FUSES LVP                   	//Low Voltage Programming on B3(PIC16) or B5(PIC18)
#FUSES NOCPD                 	//No EE protection

#use delay(clock=1000000)

