#include "H:\Eletrônica - Programação\PIC\Registradores TRIS - PORT\Code.h"     

#bit TRB0 = 0x86.0        
#bit PIN0B0 = 0x06.0   

#bit TRC0 = 0x87.0
#bit LED1 = 0x07.0
       
                  
void main()              
{

   setup_adc_ports(NO_ANALOGS);
   setup_adc(ADC_OFF);
   setup_psp(PSP_DISABLED);
   setup_spi(SPI_SS_DISABLED);
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_comparator(NC_NC_NC_NC);
   setup_vref(FALSE);

   TRB0=1;
   TRC0=0;        
   LED1=0;        
   
   while(true){            
   if(PIN0B0){
      LED1=1;      
   }else
      LED1=0;
    
   }             
      
   

}
